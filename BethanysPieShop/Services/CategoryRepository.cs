﻿using System;
using System.Collections.Generic;
using BethanysPieShop.DbContexts;
using BethanysPieShop.Models;

namespace BethanysPieShop.Services
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly AppDbContext _context;

        public CategoryRepository(AppDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IEnumerable<Category> AllCategories => _context.Categories;
    }
}
