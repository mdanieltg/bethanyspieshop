﻿using System;
using System.Collections.Generic;
using System.Linq;
using BethanysPieShop.DbContexts;
using BethanysPieShop.Models;
using Microsoft.EntityFrameworkCore;

namespace BethanysPieShop.Services
{
    public class PieRepository : IPieRepository
    {
        private readonly AppDbContext _context;

        public PieRepository(AppDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IEnumerable<Pie> AllPies => _context.Pies
            .Include(p => p.Category);

        public IEnumerable<Pie> PiesOfTheWeek => _context.Pies
            .Include(p => p.Category)
            .Where(p => p.IsPieOfTheWeek == true);

        public Pie GetPieById(int pieId)
        {
            return _context.Pies.FirstOrDefault(p => p.PieId == pieId);
        }
    }
}
