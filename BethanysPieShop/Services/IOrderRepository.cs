﻿using BethanysPieShop.Models;

namespace BethanysPieShop.Services
{
    public interface IOrderRepository
    {
        void CreateOrder(Order order);
    }
}
