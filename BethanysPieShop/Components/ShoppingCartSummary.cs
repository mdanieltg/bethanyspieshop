﻿using System;
using BethanysPieShop.Services;
using BethanysPieShop.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace BethanysPieShop.Components
{
    public class ShoppingCartSummary : ViewComponent
    {
        private readonly ShoppingCart _cart;

        public ShoppingCartSummary(ShoppingCart shoppingCart)
        {
            _cart = shoppingCart ?? throw new ArgumentNullException(nameof(shoppingCart));
        }

        public IViewComponentResult Invoke()
        {
            var items = _cart.GetShoppingCartItems();
            _cart.ShoppingCartItems = items;

            var shoppingCartViewModel = new ShoppingCartViewModel
            {
                ShoppingCart = _cart,
                ShoppingCartTotal = _cart.GetShoppingCartTotal()
            };

            return View(shoppingCartViewModel);
        }
    }
}
