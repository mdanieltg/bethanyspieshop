﻿using System;
using System.Collections.Generic;
using BethanysPieShop.DbContexts;
using BethanysPieShop.Models;

namespace BethanysPieShop.Services
{
    public class OrderRepository : IOrderRepository
    {
        private readonly AppDbContext _context;
        private readonly ShoppingCart _cart;

        public OrderRepository(AppDbContext context, ShoppingCart cart)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _cart = cart ?? throw new ArgumentNullException(nameof(cart));
        }

        public void CreateOrder(Order order)
        {
            order.OrderPlaced = DateTime.Now;
            order.OrderTotal = _cart.GetShoppingCartTotal();

            order.OrderDetails = new List<OrderDetail>();

            foreach (var cartItem in _cart.ShoppingCartItems)
            {
                var orderDetail = new OrderDetail
                {
                    Amount = cartItem.Amount,
                    PieId = cartItem.Pie.PieId,
                    Price = cartItem.Pie.Price
                };

                order.OrderDetails.Add(orderDetail);
            }

            _context.Orders.Add(order);
            _context.SaveChanges();
        }
    }
}
