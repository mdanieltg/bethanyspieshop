﻿using System;
using System.Collections.Generic;
using System.Linq;
using BethanysPieShop.Models;
using BethanysPieShop.Services;
using BethanysPieShop.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace BethanysPieShop.Controllers
{
    public class PieController : Controller
    {
        private readonly IPieRepository _pieRepository;
        private readonly ICategoryRepository _categoryRepository;

        public PieController(IPieRepository pieRepository, ICategoryRepository categoryRepository)
        {
            _pieRepository = pieRepository ?? throw new ArgumentNullException(nameof(pieRepository));
            _categoryRepository = categoryRepository ?? throw new ArgumentNullException(nameof(categoryRepository));
        }

        // public ViewResult List()
        // {
        //     var piesListViewModel = new PiesListViewModel()
        //     {
        //         Pies = _pieRepository.AllPies,
        //         CurrentCategory = "Cheese cakes"
        //     };
        //     return View(piesListViewModel);
        // }

        public ViewResult List(string category)
        {
            IEnumerable<Pie> pies;
            string currentCategory;

            if (string.IsNullOrEmpty(category))
            {
                pies = _pieRepository.AllPies
                    .OrderBy(pie => pie.PieId);
                currentCategory = "All pies";
            }
            else
            {
                pies = _pieRepository.AllPies
                    .Where(pie => pie.Category.CategoryName == category)
                    .OrderBy(pie => pie.PieId);
                currentCategory = _categoryRepository.AllCategories
                    .FirstOrDefault(c => c.CategoryName == category)?.CategoryName;
            }

            return View(new PiesListViewModel
            {
                Pies = pies,
                CurrentCategory = currentCategory
            });
        }

        public IActionResult Details(int id)
        {
            var pie = _pieRepository.GetPieById(id);
            if (pie == null) return NotFound();

            return View(pie);
        }
    }
}
