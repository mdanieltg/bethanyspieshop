﻿using System.Collections.Generic;
using BethanysPieShop.Models;

namespace BethanysPieShop.Services
{
    public interface ICategoryRepository
    {
        IEnumerable<Category> AllCategories { get; }
    }
}
