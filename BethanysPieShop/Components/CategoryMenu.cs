﻿using System;
using System.Linq;
using BethanysPieShop.Services;
using Microsoft.AspNetCore.Mvc;

namespace BethanysPieShop.Components
{
    public class CategoryMenu : ViewComponent
    {
        private readonly ICategoryRepository _repository;

        public CategoryMenu(ICategoryRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public IViewComponentResult Invoke()
        {
            var categories = _repository.AllCategories
                .OrderBy(category => category.CategoryName);

            return View(categories);
        }
    }
}
